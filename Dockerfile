FROM openjdk:7

WORKDIR usr/src/path

COPY . .

RUN javac Main.java

EXPOSE 8080

ENTRYPOINT ["/gradlew","bootRun"]
